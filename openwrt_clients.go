package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"

	"github.com/melbahja/goph"
	"github.com/o1egl/fwencoder"
	cli "github.com/urfave/cli/v2"
	gc "gitlab.com/cmulk/openwrt_getclients/getclients"
)

var l *log.Logger

func run(c *cli.Context) error {

	router := c.String("router")
	aps := c.StringSlice("aps")
	jsonFlag := c.Bool("json")

	l.Printf("ROUTER: %v\n", router)
	l.Printf("APS: %v\n", aps)
	l.Printf("JSON: %t\n", jsonFlag)

	var wClients []gc.WifiClient
	var leases []gc.DhcpLease
	privkey := os.Getenv("HOME") + "/.ssh/id_rsa"

	// Start new ssh auth with private key.
	auth, err := goph.Key(privkey, "")
	if err != nil {
		log.Fatal(err)
	}

	// Get DHCP leases from router
	l.Println("Getting DHCP leases from router...")
	leases = gc.GetDhcpLeases(auth, router)
	// pp.Println(leases)

	// Loop through APs and get WiFi clients
	l.Println("Getting WiFi clients...")
	for _, t := range aps {

		wClients = append(wClients, gc.GetWifiClients(auth, t)...)

	}

	// pp.Println(wClients)

	table := gc.GenerateOutput(leases, wClients)

	if jsonFlag {
		// Print output as json
		output, err := json.Marshal(&table)
		if err != nil {
			l.Fatal(err)
		}
		fmt.Println(string(output))

	} else {
		// Print output as fixed width columns
		output, err := fwencoder.Marshal(&table)
		if err != nil {
			l.Fatal(err)
		}
		fmt.Println(string(output))

	}
	return nil
}

// Main
func main() {
	l = log.New(os.Stderr, "| ", log.LstdFlags|log.Lmsgprefix)

	app := &cli.App{
		Name:                 "openwrt_getclients",
		Usage:                "Get local network clients",
		Action:               run,
		EnableBashCompletion: true,
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:  "router",
				Value: "router.lan",
				Usage: "The address of the router",
			},
			&cli.StringSliceFlag{
				Name:  "aps",
				Value: cli.NewStringSlice("lr.lan", "office.lan", "upstairs.lan"),
				Usage: "The addresses of the APs",
			},
			&cli.BoolFlag{
				Name:  "json",
				Value: false,
				Usage: "Output in JSON format",
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		l.Fatal(err)
	}
}
