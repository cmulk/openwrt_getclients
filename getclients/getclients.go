package getclients

import (
	"fmt"
	"log"
	"os"
	"regexp"
	"strings"

	"github.com/melbahja/goph"
)

// DhcpLease object
type DhcpLease struct {
	MacAddress string
	IPAddress  string
	Hostname   string
}

// WifiClient Object
type WifiClient struct {
	MacAddress string
	WifiName   string
	ApName     string
	Signal     string
	Channel    string
}

// OutputRow for output table
type OutputRow struct {
	MacAddress string `column:"MAC Address"`
	IPAddress  string `column:"IP Address"`
	Hostname   string `column:"Hostname"`
	WifiName   string `column:"WiFi Name"`
	ApName     string `column:"AP Name"`
	Signal     string `column:"Signal"`
	Channel    string `column:"Channel"`
}

// GetDhcpLeases - get DHCP leases from the router (target)
func GetDhcpLeases(auth goph.Auth, target string) []DhcpLease {

	var leases []DhcpLease

	client, err := goph.New("root", target, auth)
	if err != nil {
		log.Fatal(err)
	}

	// Defer closing the network connection.
	defer client.Close()

	// Read dhcp.leases file
	out, err := client.Run("cat /tmp/dhcp.leases")
	if err != nil {
		log.Fatal(err)
	}

	// Trim trailing space and split dhcp.leases string int lines
	lines := strings.Split(strings.TrimSpace(string(out)), "\n")

	// Loop through each line, split into fields and add object to return slice
	for _, v := range lines {
		fields := strings.Fields(v)
		leases = append(leases, DhcpLease{
			MacAddress: strings.ToUpper(fields[1]),
			IPAddress:  fields[2],
			Hostname:   fields[3],
		})

	}

	return leases
}

// GetWifiClients - get wifi clients from an AP (target)
func GetWifiClients(auth goph.Auth, target string) []WifiClient {

	var wclient []WifiClient
	l := log.New(os.Stderr, "", log.LstdFlags)

	client, err := goph.New("root", target, auth)
	if err != nil {
		l.Printf("Error: %s", err)
		l.Printf("Failed to connect to %s, skipping...", target)
		return []WifiClient{}
	}

	// Defer closing the network connection.
	defer client.Close()

	// Run iwinfo to get list of wireless interfaces
	out, err := client.Run("iwinfo")
	if err != nil {
		log.Fatal(err)
	}

	// Use regex to pull out all wireless interfaces and ESSIDs from iwinfo
	// make regex treat all output as single line with (?s) so that .+ also crosses newlines
	re := regexp.MustCompile(`(?s)([a-zA-Z0-9\-]+)\s+?ESSID: "([a-zA-Z0-9\-]+)".+?Channel: ([0-9]+)`)
	matches := re.FindAllStringSubmatch(string(out), -1)

	// loop through wireless interfaces
	for _, wl := range matches {

		// Use iwinfo again to get list of associated clients for the wifi interface
		out, err := client.Run(fmt.Sprintf("iwinfo %s assoclist", wl[1]))
		if err != nil {
			log.Fatal(err)
		}

		// Use Regex to pull out all client MAC addresses to a list
		re := regexp.MustCompile(`(?m)^(([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2}))\s+([0-9\-]{3} dBm)`)
		clientInfo := re.FindAllStringSubmatch(string(out), -1)

		// Loop through all client MAC addresses and create objects for return slice
		for _, m := range clientInfo {
			wclient = append(wclient, WifiClient{
				MacAddress: strings.ToUpper(m[1]),
				WifiName:   wl[2],
				ApName:     target,
				Signal:     m[4],
				Channel:    wl[3],
			})
		}

	}

	return wclient
}

// GenerateOutput - generate the final output structure
func GenerateOutput(leases []DhcpLease, wClients []WifiClient) []OutputRow {

	var table []OutputRow
	var found bool
	// Loop through wifi clients and match with DHCP info to create final output
	for i := range wClients { // Loop through wifi clients
		found = false
		for j := range leases { // Search for matching DHCP lease

			if leases[j].MacAddress == wClients[i].MacAddress {
				// Found matching DHCP lease, create output row and end search
				table = append(table, OutputRow{
					MacAddress: leases[j].MacAddress,
					IPAddress:  leases[j].IPAddress,
					Hostname:   leases[j].Hostname,
					WifiName:   wClients[i].WifiName,
					ApName:     wClients[i].ApName,
					Signal:     wClients[i].Signal,
					Channel:    wClients[i].Channel,
				})
				found = true
				break
			}
		}
		// No matching DHCP lease found, create output row without that info
		if !found {
			table = append(table, OutputRow{
				MacAddress: wClients[i].MacAddress,
				IPAddress:  "NONE",
				Hostname:   "NONE",
				WifiName:   wClients[i].WifiName,
				ApName:     wClients[i].ApName,
				Signal:     wClients[i].Signal,
				Channel:    wClients[i].Channel,
			})
		}
	}

	// Find other DHCP leases that are not wifi and add them to the list
	for i := range leases {
		found = false
		for j := range wClients {
			if leases[i].MacAddress == wClients[j].MacAddress {
				found = true
				break
			}

		}
		if !found {
			table = append(table, OutputRow{
				MacAddress: leases[i].MacAddress,
				IPAddress:  leases[i].IPAddress,
				Hostname:   leases[i].Hostname,
				WifiName:   "NONE",
				ApName:     "NONE",
				Signal:     "NONE",
				Channel:    "NONE",
			})
		}
	}

	return table
}
